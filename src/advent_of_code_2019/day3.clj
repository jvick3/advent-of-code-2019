(ns advent-of-code-2019.day3
  (require [clojure.string :refer [split-lines]] 
           [clojure.set :refer [intersection]]))

; approach idea: convert wires to coordinates
; then find set intersection of them and sort
; by manhattan dist

(defn line-trace [[x y] desc]
  (let [dir (first desc)
        amount (Integer/parseInt (.substring desc 1))]
    (case dir
      \L (for [x' (range (dec x) (- x amount 1) -1)] [x' y])
      \R (for [x' (range (inc x) (+ x amount 1))]    [x' y])
      \D (for [y' (range (dec y) (- y amount 1) -1)] [x y'])
      \U (for [y' (range (inc y) (+ y amount 1))]    [x y'])
      (throw (Exception. (str "Unknown dir " dir))))))

(defn traces [desc-str]
  (let [descs (.split desc-str ",")]
    (loop [pos [0 0]
           descs descs
           traces []]
      (if (empty? descs)
        traces
        (let [lt (line-trace pos (first descs))]
          (recur (last lt) (rest descs) (concat traces lt)))))))

(defn mdist
  [[^int ^int x1 ^int y1] [^int x2 ^int y2]]
  (+ (Math/abs (- x1 x2))
     (Math/abs (- y1 y2))))

(defn parse-traces [f]
  (map traces (split-lines (slurp f))))

(defn crossings
  ([filename]
   (let [[trace-1 trace-2] (parse-traces filename)]
     (crossings trace-1 trace-2)))
  ([trace-1 trace-2]
   (intersection (set trace-1) (set trace-2))))

(defonce ^:private test-file "resources/day3_input.txt")

(defn solve-1 []
  (let [dist-fn (partial mdist [0 0])]
    (apply min (map dist-fn (crossings test-file)))))
         
(comment (solve-1)) ; => 529

(defn steps-to-crossings 
  [trace-1 trace-2 crossings]
  (for [c crossings]
    ; increment to include intersection itself
    [(inc (.indexOf trace-1 c))
     (inc (.indexOf trace-2 c))]))

(defn min-combined-steps-to-intersect
  [trace-1 trace-2]
  (let [crossings (crossings trace-1 trace-2)
        s-to-c (steps-to-crossings trace-1 trace-2 crossings)
        combined-steps (map (partial apply +) s-to-c)]
    (apply min combined-steps)))

(defn solve-2 []
  (let [[trace-1 trace-2] (parse-traces test-file)]
    (min-combined-steps-to-intersect trace-1 trace-2)))

(comment (solve-2)) ; => 20386