(ns advent-of-code-2019.day1)

(defn fuel-required
  [mass]
  (int (- (Math/floor (/ mass 3)) 2)))

(defn read-input [f]
  (->> (slurp f)
      (clojure.string/split-lines)
      (map #(Integer/parseInt %))))

(defn solution [fuel-func]
  (->> "resources/day1_input.txt"
     read-input
     (map fuel-func)
     (reduce +)))

; Solution to part 1
(comment (solution fuel-required))  ; => 3271994

(defn fuel-required-accounting-for-fuel
  [mass]
  (loop [remaining mass
         total 0]
    (let [fuel (fuel-required remaining)]
      (if (<= fuel 0)
        total
        (recur fuel (+ total fuel))))))

; Solution to part 2
(comment (solution fuel-required-accounting-for-fuel)) ; => 4905116

;; TODO get TDD setup going with Midje/reloading and write test cases for these