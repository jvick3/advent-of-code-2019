(ns advent-of-code-2019.day2)

(defonce ^:private test-file "resources/day2_input.txt")
(defonce ^:private stride 4)

(defn read-input [f]
  (as-> (slurp f) $
      (.split $ ",")
      (map #(Integer/parseInt %) $)
      (vec $)))

(defmulti op identity)
(defmethod op 1 [_] +)
(defmethod op 2 [_] *)

(defn execute
  [program]
  (loop [program program
         index 0]
    (if (>= (+ index stride) (count program))
      program
      (let [[opcode pos1 pos2 out-pos] (subvec program index (+ index stride))]
        (if (= opcode 99)
          program
          (let [operator (op opcode)
                computed (operator (program pos1) (program pos2))
                updated-program (assoc program out-pos computed)]
            (recur updated-program (+ index stride))))))))

(defn execution-output [program]
  (first (execute program)))

; Solution to part 1
(defn solve-1 []
  (-> test-file
      read-input
      (assoc , 1 12 2 2)
      execution-output))

(comment (solve-1)) ; => 3409710
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn find-input-pair
  [program target]
  (let [pred #(= target (execution-output (assoc program 1 %1 2 %2)))
        vals (range 100)]
    (first (for [noun vals verb vals
                 :when (pred noun verb) :while (pred noun verb)]
             [noun verb]))))

; Part 2
(defn solve-2 []
  (as-> test-file $ 
        (read-input $)
        (find-input-pair $ 19690720)
        (+ (* 100 (first $)) (second $))))

(comment (solve-2)) ; => 7912