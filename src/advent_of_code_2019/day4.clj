(ns advent-of-code-2019.day4)

(defn digits [num] 
  (map #(Character/digit % 10) (str num)))

(defn increasing? [nums]
  (apply <= nums))

(defn adjacent-equal-pairs [coll] 
  (let [adjacent-pairs (partition 2 1 coll)]
    (filter #(= (first %) (second %)) adjacent-pairs)))

(defn possible-code? [code]
  (let [digs (digits code)]
    (and (increasing? digs)
         (not (empty? (adjacent-equal-pairs digs))))))

(defn possible-codes [lower upper pred]
  (count (filter pred (range lower (inc upper))))) ; inclusive range

(defonce ^:private min-code 265275)
(defonce ^:private max-code 781584)

; solution to part 1
(defn solve-1 [] 
  (possible-codes min-code max-code possible-code?))

(defn possible-code-2? [code]
  (let [digs (digits code)
        freqs (frequencies digs)
        adj-eq-pairs (adjacent-equal-pairs digs)]
    (and (increasing? digs)
         (not (empty? adj-eq-pairs))
         (some #(= 2 (get freqs (first %))) adj-eq-pairs))))

(defn solve-2 []
  (possible-codes min-code max-code possible-code-2?))