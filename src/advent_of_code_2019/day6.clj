(ns advent-of-code-2019.day6)

(defn read-lines [f]
  (clojure.string/split-lines (slurp f)))

(defn parse-orbit [orbit-str]
  (let [[orbited orbiter] (.split orbit-str "\\)")]
    {orbiter orbited}))

(defn parse-orbit-file [f]
  (->> f
       read-lines
       (map parse-orbit)
       (into {})))

(defn num-paths
  ([m k]
   (loop [n 0
          current-k k]
     (if current-k
       (recur (inc n) (get m current-k))
       (dec n))))
  ([m] 
   (reduce + 
           (map (partial num-paths m) (keys m)))))

; solution to part 1
(num-paths (parse-orbit-file "resources/day6_input.txt")) ; => 147807

