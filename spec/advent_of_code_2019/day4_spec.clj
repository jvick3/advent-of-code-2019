(ns advent-of-code-2019.day4-spec
  (:require [speclj.core :refer :all]
            [advent-of-code-2019.day4 :refer :all]))

(describe "digits"
  (it "Returns (1 2 3 4) for 1234"
    (should= '(1 2 3 4) (digits 1234)))
  (it "Returns (5 2 6) for 526"
      (should= '(5 2 6) (digits 526))))

(describe "increasing?"
  (it "Returns true when it should"
    (should= true (increasing? [1]))
    (should= true (increasing? [1 1]))
    (should= true (increasing? [1 2 3]))
    (should= true (increasing? [2 4 6 7 10])))
  (it "Returns false when it should"
    (should= false (increasing? [2 1]))
    (should= false (increasing? [1 2 3 2]))))

(describe "adjacent-equal-pairs"
  (it "just works"
    (should= '((1 1)) (adjacent-equal-pairs [1 1]))
    (should= '((2 2) (3 3) (6 6) (6 6)) 
      (adjacent-equal-pairs [1 2 2 3 3 4 5 6 6 6]))))

(describe "possible-code?"
  (it "Returns true when it should"
    (should (possible-code? 111111)))
  (it "Returns false when it should"
    (should-not (possible-code? 223450))
    (should-not (possible-code? 123789))))

(describe "solve-1"
  (it "returns known answer"
    (should= 960 (solve-1))))

(describe "possible-code-2?"
  (it "Returns true when it should"
    (should (possible-code-2? 112233))
    (should (possible-code-2? 111122)))
  (it "Returns false when it should"
    (should-not (possible-code-2? 123444))))

(describe "solve-2"
  (it "returns known answer"
    (should= 626 (solve-2))))

(run-specs)