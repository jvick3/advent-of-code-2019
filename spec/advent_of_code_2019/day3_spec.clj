(ns advent-of-code-2019.day3-spec
  (:require [speclj.core :refer :all]
            [advent-of-code-2019.day3 :refer :all]))

(describe "Manhattan distance - mdist"
  (it "Returns 0 for same coordinate"
    (should= 0 (mdist [0 0] [0 0])))
  (it "Returns 1 for [0 0] [1 0]"
      (should= 1 (mdist [0 0] [1 0])))
  (it "Works with mixtures negative numbers also"
      (should= 7 (mdist [-1 2] [1 -3]))))

(describe "traces"
  (it "Works for simple case involving all directions"
    (should= (traces "R1,U1,L2,D1") 
             [[1 0] [1 1] [0 1] [-1 1] [-1 0]])))

(describe "Solution to part 1"
  (it "Works for part 1"
      (should= 529 (solve-1))))

(describe "Solution to part 2"
  (it "Works for part 2"
      (should= 20386 (solve-2))))

(run-specs)