(ns advent-of-code-2019.day6-spec
  (:require [speclj.core :refer :all]
            [advent-of-code-2019.day6 :refer :all]))

(describe "parse-orbit"
  (it "Returns map from orbiter => orbited"
    (should= {"b" "a"} (parse-orbit "a)b"))
    (should= {"Q4Q" "SGZ"} (parse-orbit "SGZ)Q4Q"))))

(def test-orbits
  {"B" "COM" "C" "B" "D" "C" "E" "D"
   "F" "E" "G" "B" "H" "G" "I" "D"
   "J" "E" "K" "J" "L" "K"})

(num-paths test-orbits "D")

(describe "parse-orbit-file"
  (it "works for test file"
    (should= test-orbits (parse-orbit-file "resources/day6_test_input.txt"))))

(describe "num-paths"
  (it "Works for known test cases"
    (should= 42 (num-paths test-orbits))
    (should= 147807 
             (num-paths (parse-orbit-file "resources/day6_input.txt")))))

(run-specs)